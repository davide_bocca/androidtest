package com.android.davidebocca.myfirstapplication.service.impl;

import android.content.Context;

import com.android.davidebocca.myfirstapplication.dao.MyDao;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.service.LoginService;
import com.android.davidebocca.myfirstapplication.util.Constants;

/**
 * Created by davide.bocca on 13/08/2014.
 */
public class LoginServiceImpl implements LoginService {

    private static LoginServiceImpl instance;
    private static MyDao myDao;

    @Override
    public UserModel login(String username, String password, Context context) {
        UserModel userModel;
        userModel = myDao.doLogin(username,password, context);
        return userModel;
    }

    public synchronized static LoginServiceImpl get() {
        if (instance == null) {
            instance = new LoginServiceImpl();
        }
        return instance;
    }

    public void closeDb(){
        myDao.close();
    }

    public void openDb(Context context){
        if(myDao == null){
            myDao = MyDao.get(context);
        }
        myDao.open();
    }
}
