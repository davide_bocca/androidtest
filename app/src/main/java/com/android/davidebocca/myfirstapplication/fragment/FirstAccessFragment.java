package com.android.davidebocca.myfirstapplication.fragment;



import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.util.Constants;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class FirstAccessFragment extends Fragment {

    private Button anonymousButton;
    private Button loginButton;
    private Button registerButton;

    private AssetManager assets;
    private Typeface angryBirdsButtonFont;

    public FirstAccessFragment() {
        // Required empty public constructor
    }

    public interface FirstAccessListener {
        void enterAsAnonymous();
        void doLogin();
        void doRegistration();
    }

    private FirstAccessListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        assets = activity.getAssets();
        angryBirdsButtonFont = Typeface.createFromAsset(assets, Constants.ANGRY_BIRDS_FONT_PATH);
        if(activity instanceof FirstAccessListener){
            mListener = (FirstAccessListener) activity;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View firstAccessView = inflater.inflate(R.layout.fragment_first_access,null);

        anonymousButton = (Button) firstAccessView
                .findViewById(R.id.anonymous_user);

        anonymousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.enterAsAnonymous();
                }
            }
        });

        loginButton = (Button) firstAccessView
                .findViewById(R.id.login_user);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.doLogin();
                }
            }
        });

        registerButton = (Button) firstAccessView
                .findViewById(R.id.register_user);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.doRegistration();
                }
            }
        });

        anonymousButton.setTypeface(angryBirdsButtonFont);
        loginButton.setTypeface(angryBirdsButtonFont);
        registerButton.setTypeface(angryBirdsButtonFont);

        return firstAccessView;
    }

    @Override
    public void onDetach(){
        super.onDetach();
        this.mListener = null;
    }

}
