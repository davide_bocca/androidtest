package com.android.davidebocca.myfirstapplication.listener;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import com.android.davidebocca.myfirstapplication.R;

import javax.xml.transform.Result;

/**
 * Created by davide.bocca on 27/08/2014.
 */
public class NoFragmentRemoveTabListener extends MasterTabListener {

    private Fragment mFragment;
    private boolean mFirtsView;

    private final static String TAG_LOG = NoFragmentRemoveTabListener.class.getName();

    public NoFragmentRemoveTabListener(Fragment fragment, Context context){
        super(context);
        this.mFragment = fragment;
        this.mFirtsView = true;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        super.onTabSelected(tab,ft);
        if(mFirtsView){
            ft.add(R.id.anchor_container, mFragment);
            mFirtsView = false;
        }
        else {
            ft.show(mFragment);
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        super.onTabUnselected(tab,ft);
        ft.hide(mFragment);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        super.onTabReselected(tab,ft);
    }

}
