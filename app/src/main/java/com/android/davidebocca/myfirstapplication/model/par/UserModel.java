package com.android.davidebocca.myfirstapplication.model.par;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.android.davidebocca.myfirstapplication.util.Constants;

import java.util.Date;

/**
 * Created by davide.bocca on 11/08/2014.
 */
public class UserModel implements Parcelable {

    private long birthDate;
    private String mUserName;
    private String mEmail;
    private String mLocation;
    private String mPassword;
    private String mId;

    private final static byte PRESENT = 1;
    private final static byte NOT_PRESENT = 0;

    private UserModel(long birthDate) {
        this.birthDate = birthDate;
    }

    public static UserModel create(final long birthDate){
        final UserModel userModel = new UserModel(birthDate);
        return userModel;
    }

    public UserModel withEmail(String email){
        if (email == null) {
            throw new IllegalArgumentException("Email cannot be null here!");
        }
        mEmail = email;
        return this;
    }

    public String getmPassword() {
        return mPassword;
    }

    public UserModel withUsername(String username){
        if (username == null) {
            throw new IllegalArgumentException("Username cannot be null here!");
        }
        mUserName = username;
        return this;

    }

    public String getmId() {
        return mId;
    }

    public UserModel withId(String id){
        mId = id;
        return this;
    }

    public UserModel withPassword(String password){
        if (password == null) {
            throw new IllegalArgumentException("Password cannot be null here!");
        }
        mPassword = password;
        return this;
    }

    public UserModel withLocation(String location){
        if (location == null) {
            throw new IllegalArgumentException("Location cannot be null here!");
        }
        mLocation = location;
        return this;
    }

    public boolean isAnonymous(){
        return TextUtils.isEmpty(mUserName);
    }

    public boolean isLogged(){
        return !TextUtils.isEmpty(mUserName);
    }

    public String getmUserName() {return mUserName;}

    public String getBirthDate (){
        Date date = new Date(this.birthDate);
        return date.toString();
    }

    public String getmLocation() {return mLocation;}

    public String getmEmail() {return mEmail;}

    public static UserModel fromCursor(Cursor cursor){
        long birthDate = cursor.getLong(cursor.getColumnIndex(Constants.DB_USER_BIRTHDATE_COLUMN));
        String userName = cursor.getString(cursor.getColumnIndex(Constants.DB_USER_USERNAME_COLUMN));
        String email = cursor.getString(cursor.getColumnIndex(Constants.DB_USER_EMAIL_COLUMN));
        String location = cursor.getString(cursor.getColumnIndex(Constants.DB_USER_LOCATION_COLUMN));
        String id = cursor.getString(cursor.getColumnIndex(Constants.DB_USER_ID_COLUMN));

        return UserModel.create(birthDate)
                .withUsername(userName)
                .withEmail(email)
                .withLocation(location)
                .withId(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(birthDate);

        if(!TextUtils.isEmpty(mUserName)){
            parcel.writeByte(PRESENT);
            parcel.writeString(mUserName);
        }
        else {
            parcel.writeByte(NOT_PRESENT);
        }

        if(!TextUtils.isEmpty(mEmail)){
            parcel.writeByte(PRESENT);
            parcel.writeString(mEmail);
        }
        else {
            parcel.writeByte(NOT_PRESENT);
        }

        if(!TextUtils.isEmpty(mLocation)){
            parcel.writeByte(PRESENT);
            parcel.writeString(mLocation);
        }
        else {
            parcel.writeByte(NOT_PRESENT);
        }

        if(!TextUtils.isEmpty(mId)){
            parcel.writeByte(PRESENT);
            parcel.writeString(mId);
        }
        else {
            parcel.writeByte(NOT_PRESENT);
        }
    }

    public UserModel(Parcel in){
        this.birthDate = in.readLong();
        
        if(in.readByte() == PRESENT){
            this.mUserName = in.readString();
        }

        if(in.readByte() == PRESENT){
            this.mEmail = in.readString();
        }

        if(in.readByte() == PRESENT){
            this.mLocation = in.readString();
        }

        if(in.readByte() == PRESENT){
            this.mId = in.readString();
        }
    }

    public static final Creator<UserModel> CREATOR =
        new Creator<UserModel>(){

            @Override
            public UserModel createFromParcel(Parcel parcel) {
                return new UserModel(parcel);
            }

            @Override
            public UserModel[] newArray(int i) {
                return new UserModel[i];
            }
        };
}
