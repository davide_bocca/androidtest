package com.android.davidebocca.myfirstapplication.service;

import android.content.Context;

import com.android.davidebocca.myfirstapplication.model.par.UserModel;

/**
 * Created by davide.bocca on 13/08/2014.
 */
public interface LoginService {
    public UserModel login(final String username, final String password, final Context context);
}
