package com.android.davidebocca.myfirstapplication.service;

import android.content.Context;

import com.android.davidebocca.myfirstapplication.exception.MyException;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;

/**
 * Created by davide.bocca on 18/08/2014.
 */
public interface RegisterService {
    public UserModel register(String username, String password, String email, long birthDate, String location) throws MyException;
}
