package com.android.davidebocca.myfirstapplication.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.nfc.Tag;
import android.util.Log;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.exception.MyException;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by davide.bocca on 25/08/2014.
 */
public class MyDao {

    private Context mContext;
    private SQLiteDatabase mDb;

    private static final String TAG_LOG = MyDao.class.getName();

    public MyDao(Context applicationContext) {
        mContext = applicationContext;
    }

    public static synchronized MyDao get(final Context context){
        return new MyDao(context.getApplicationContext());
    }

    public void open(){
        if(mDb == null || !mDb.isOpen()){
            final File mDbFile = mContext.getDatabasePath(Constants.DB_NAME);
            final boolean dbExisting = mDbFile.exists();
            if(!dbExisting){
                final String createDbStatement;
                try {
                    Log.d(TAG_LOG, "DB not exists, trying to create");
                    createDbStatement = ResourceUtils.getStringFromRawResource(mContext,R.raw.create_schema);
                    DatabaseUtils.createDbFromSqlStatements(mContext,
                            Constants.DB_NAME,
                            Constants.DB_VERSION,
                            createDbStatement);
                    Log.d(TAG_LOG, "DB created");
                } catch (IOException e) {
                    Log.e(TAG_LOG, "Error getting schema file");
                }
            }
            mDb = mContext.openOrCreateDatabase(Constants.DB_NAME,
                    Context.MODE_PRIVATE,
                    null);
            int oldVersion = mDb.getVersion();
            if(oldVersion != Constants.DB_VERSION){

            }
        }
    }

    public void close(){
        if(mDb != null && mDb.isOpen()){
            mDb.close();
        }
    }

    public long insertUser(UserModel userModel) throws MyException {

        long id = -1;

        ContentValues values = new ContentValues();

        values.put(Constants.DB_USER_USERNAME_COLUMN,userModel.getmUserName());
        values.put(Constants.DB_USER_EMAIL_COLUMN,userModel.getmEmail());
        values.put(Constants.DB_USER_BIRTHDATE_COLUMN,userModel.getBirthDate());
        values.put(Constants.DB_USER_LOCATION_COLUMN,userModel.getmLocation());
        values.put(Constants.DB_USER_PASSWORD_COLUMN,userModel.getmPassword());

        try {
            id = mDb.insertOrThrow(Constants.DB_USER_TABLE, null, values);
        } catch (SQLiteConstraintException e) {
            throw new MyException("Username not available",e);
        }

        return id;
    }

    public UserModel doLogin(String username, String password, Context context){
        String sql = ResourceUtils.getPropertyFromRawPropertiesFile(R.raw.query, "get_user_from_username_password", context);
        Log.d(TAG_LOG, "SQL From properties file -> " + sql);
        Cursor cursor = mDb.rawQuery(sql,new String[]{username,password});

        if(cursor.getCount() == 1){
            cursor.moveToNext();
            return UserModel.fromCursor(cursor);
        }
        return null;
    }
}
