package com.android.davidebocca.myfirstapplication.service;

import com.android.davidebocca.myfirstapplication.model.AdapterValueModel;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;

import java.util.List;

/**
 * Created by davide.bocca on 19/08/2014.
 */
public interface AdapterValueService {
    public List<AdapterValueModel> getValues(int customNumber);
}
