package com.android.davidebocca.myfirstapplication.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Created by davide.bocca on 13/08/2014.
 */
public class Constants {

    public final static String PKG = "com.android.davidebocca.myfirstapplication";
    public final static String USERNAME = "davide";
    public final static String PASSWORD = "davide";
    public final static String USER_DATA_EXTRA = "user_data_login";
    public final static String USER_DATA_RECAP = "user_data_recap";
    public final static String USER_MAIN_ACTIVITY_EXTRA = "user_main_activity";
    public final static String LOGIN_ACTION = PKG + ".action.LOGIN_ACTION";
    public final static String REGISTRATION_ACTION = PKG + ".action.REGISTRATION_ACTION";
    public final static String SHOW_USER_DATA_ACTION = PKG + ".action.SHOW_USER_DATA_ACTION";
    public final static String ANGRY_BIRDS_FONT_PATH = "fonts/angrybirds-regular.ttf";
    public final static boolean MUTE_APP = false;
    public final static String SHARED_PREFERENCES = "davide_bocca_shared_preferences";
    public final static DateFormat DATE_FORMAT = new SimpleDateFormat("E dd MMMM yyyy");

    public final static String PICTURE_FOLDER = "Android Test";
    public final static String TAKEN_IMAGE_PATH_EXTRA = "taken_image_path";
    public final static String TAKEN_IMAGE_URI_EXTRA = "taken_image_uri";

    public final static String SELECTED_TAB_INDEX_PARAM = "selected_tab_index";

//    DATABASE
    public final static String DB_NAME = "davide_db";
    public final static String DB_USER_TABLE = "UTENTI";

    public final static String DB_USER_USERNAME_COLUMN = "username";
    public final static String DB_USER_EMAIL_COLUMN = "email";
    public final static String DB_USER_BIRTHDATE_COLUMN = "birthdate";
    public final static String DB_USER_LOCATION_COLUMN = "location";
    public final static String DB_USER_PASSWORD_COLUMN = "password";
    public final static String DB_USER_ID_COLUMN = "_id";

    public final static int DB_VERSION = 1;
    public final static String DB_PROPERTIES_FILE = "query.properties";

    public static final int LOGIN_REQUEST_ID = 1;
    public static final int REGISTRATION_REQUEST_ID = 2;
    public static final int TAKE_PICTURE_REQUEST_ID = 2;

    public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg", "png");
}
