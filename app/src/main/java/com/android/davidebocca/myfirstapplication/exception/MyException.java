package com.android.davidebocca.myfirstapplication.exception;

/**
 * Created by davide.bocca on 26/08/2014.
 */
public class MyException extends Exception {
    public MyException() { super(); }
    public MyException(String message) { super(message); }
    public MyException(String message, Throwable cause) { super(message, cause); }
    public MyException(Throwable cause) { super(cause); }
}
