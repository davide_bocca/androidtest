package com.android.davidebocca.myfirstapplication.fragment;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.TabNavigationActivity;
import com.android.davidebocca.myfirstapplication.adapter.GalleryImageAdapter;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.ImageUtils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class GalleryFragment extends Fragment {

    private final static String TAG_LOG = GalleryFragment.class.getName();

    private GridView mGridView;

    private GalleryImageAdapter mGalleryImageAdapter;
    private List<String> mImagePath;

    public GalleryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach (Activity activity){
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);
        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();
        prepareGallery();
    }

    @Override
    public void onResume (){
        super.onResume();
        loadGallery();
    }

    public static GalleryFragment getInstance(){
        GalleryFragment galleryFragment = new GalleryFragment();
        return galleryFragment;
    }

    private void loadGallery(){
        Log.d(TAG_LOG, "Loading gallery...");
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                Constants.PICTURE_FOLDER);

        // check for directory
        if (storageDir.isDirectory()) {
            // getting list of file paths
            File[] listFiles = storageDir.listFiles();
            // Check for count
            if (listFiles.length > 0) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mImagePath.clear();
                // loop through all files
                for (int i = 0; i < listFiles.length; i++) {
                    // get file path
                    String filePath = listFiles[i].getAbsolutePath();
                    if(ImageUtils.isSupportedFile(filePath)) {
                        mImagePath.add(filePath);
                    }
                }
            } else {
                // image directory is empty
                Toast.makeText(
                        getActivity().getApplicationContext(),
                        Constants.PICTURE_FOLDER
                                + " is empty. Please load some images in it !",
                        Toast.LENGTH_SHORT).show();
            }

        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity().getApplicationContext());
            alert.setTitle("Error!");
            alert.setMessage(Constants.PICTURE_FOLDER
                    + " directory path is not valid! Please set the image directory name AppConstant.java class");
            alert.setPositiveButton("OK", null);
            alert.show();
        }

        mGalleryImageAdapter.notifyDataSetChanged();
    }

    private void prepareGallery(){
        mGridView = (GridView) getActivity().findViewById(R.id.gridView);
        mImagePath = new LinkedList<String>();

        mGalleryImageAdapter = new GalleryImageAdapter(mImagePath,new WeakReference<Activity>(getActivity()));

        mGridView.setAdapter(mGalleryImageAdapter);
    }

}
