package com.android.davidebocca.myfirstapplication.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.util.SharedPrefUtils;

import java.util.Locale;

public class MasterActivity extends Activity {

//    private static final String TAG_LOG = MasterActivity.class.getName();
    private static final String TAG_LOG = "LIFECYCLE";

    private Toast toast;
    private View toastLayout;
    private static String activeFragment = null;
    ActionBarDrawerToggle drawerToggle = null;
    MenuItem locale_icon;
    MenuItem logged_area;

    private Locale textToSpeechLocale = Locale.ITALIAN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG_LOG,"MasterActivity -> ON CREATE");
        configToast();
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart(){
        Log.d(TAG_LOG,"MasterActivity -> ON START");
        super.onStart();
    }

    @Override
    protected void onResume(){
        Log.d(TAG_LOG,"MasterActivity -> ON RESUME");
        super.onResume();
    }

    @Override
    protected void onPause(){
        Log.d(TAG_LOG,"MasterActivity -> ON PAUSE");
        super.onPause();
    }

    @Override
    protected void onStop(){
        Log.d(TAG_LOG,"MasterActivity -> ON STOP");
        super.onStop();
        toast.cancel();
    }

    @Override
    protected void onDestroy(){
        Log.d(TAG_LOG,"MasterActivity -> ON DESTROY");
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_custom, menu);
        locale_icon = menu.findItem(R.id.locale_flag);
        updateLocaleIcon();
        if(activeFragment != null){
            if(activeFragment.equals(getApplicationContext().getString(R.string.ACTUAL_BIMBI)) || activeFragment.equals(getApplicationContext().getString(R.string.ACTUAL_MAP))){
                locale_icon.setVisible(true);
            }
            else {
                locale_icon.setVisible(false);
            }
        }

        String loggedUsername = SharedPrefUtils.get(getApplicationContext()).loggedUsername();
        logged_area = menu.findItem(R.id.action_logged_username);
        if(!TextUtils.isEmpty(loggedUsername)){
            logged_area.setTitle(getString(R.string.action_logged_username,loggedUsername));
            logged_area.setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items

        if (drawerToggle != null && drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.locale_flag:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.dialog_select_language)
                        .setItems(R.array.languages, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        textToSpeechLocale = Locale.ITALIAN;
                                    break;
                                    case 1:
                                        textToSpeechLocale = Locale.ENGLISH;
                                    break;
                                    case 2:
                                        textToSpeechLocale = Locale.GERMAN;
                                    break;
                                    case 3:
                                        textToSpeechLocale = Locale.FRENCH;
                                        break;
                                    case 4:
                                        textToSpeechLocale = Locale.CHINESE;
                                    break;
                                    case 5:
                                        textToSpeechLocale = Locale.JAPANESE;
                                    break;
                                    case 6:
                                        textToSpeechLocale = Locale.KOREAN;
                                    break;
                                }
                            updateLocaleIcon();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateLocaleIcon(){
        if(textToSpeechLocale == Locale.ITALIAN){
            locale_icon.setIcon(getResources().getDrawable(R.drawable.italy));
        }
        else if(textToSpeechLocale == Locale.ENGLISH){
            locale_icon.setIcon(getResources().getDrawable(R.drawable.england));
        }
        else if(textToSpeechLocale == Locale.GERMAN){
            locale_icon.setIcon(getResources().getDrawable(R.drawable.germany));
        }
        else if(textToSpeechLocale == Locale.FRENCH){
            locale_icon.setIcon(getResources().getDrawable(R.drawable.france));
        }
        else if(textToSpeechLocale == Locale.CHINESE){
            locale_icon.setIcon(getResources().getDrawable(R.drawable.china));
        }
        else if(textToSpeechLocale == Locale.JAPANESE){
            locale_icon.setIcon(getResources().getDrawable(R.drawable.japan));
        }
        else if(textToSpeechLocale == Locale.KOREAN){
            locale_icon.setIcon(getResources().getDrawable(R.drawable.south_korea));
        }
    }

    private void configToast(){

        LayoutInflater inflater = getLayoutInflater();

        toastLayout = inflater.inflate(R.layout.custom_toast, (ViewGroup) findViewById(R.id.toast_layout_root));

        toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(toastLayout);
    }

    public void makeToast(View v){
        RadioButton clicked = (RadioButton)v;
        makeToastWithMessage("Hai cliccato " + clicked.getText());
    }

    public void makeToastWithMessage(String message){

        TextView text = (TextView) toastLayout.findViewById(R.id.toast_text);
        text.setText(message);

        toast.show();
    }

    public void setActiveFragment(String activeFragment) {
        this.activeFragment = activeFragment;
    }

    public Locale getTextToSpeechLocale() {
        return textToSpeechLocale;
    }
}
