package com.android.davidebocca.myfirstapplication.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.android.davidebocca.myfirstapplication.R;

/**
 * Created by davide.bocca on 26/08/2014.
 */
public class SharedPrefUtils {

    private static Context mContext;

    public SharedPrefUtils(Context context) {
        mContext = context;
    }

    public String loggedUsername() {
        final String loggedUsername;
        SharedPreferences sharedPref = mContext.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        loggedUsername = sharedPref.getString(mContext.getString(R.string.logged_username), "");
        return loggedUsername;
    }

    public boolean alreadyOpenApp(){
        SharedPreferences sharedPref = mContext.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        boolean alreadyOpened = sharedPref.getBoolean(mContext.getString(R.string.first_app_opening),false);
        return alreadyOpened;
    }

    public boolean isUserLogged(){
        return !TextUtils.isEmpty(loggedUsername());
    }

    public static SharedPrefUtils get(Context context){
        return new SharedPrefUtils(context);
    }
}
