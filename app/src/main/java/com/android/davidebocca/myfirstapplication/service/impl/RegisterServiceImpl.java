package com.android.davidebocca.myfirstapplication.service.impl;

import android.content.Context;

import com.android.davidebocca.myfirstapplication.dao.MyDao;
import com.android.davidebocca.myfirstapplication.exception.MyException;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.service.RegisterService;

/**
 * Created by davide.bocca on 18/08/2014.
 */
public class RegisterServiceImpl implements RegisterService {

    private static RegisterServiceImpl instance;
    private static MyDao myDao;

    @Override
    public UserModel register(String username, String password, String email, long birthDate, String location) throws MyException {

        final UserModel userModel = UserModel
                .create(birthDate)
                .withEmail(email)
                .withUsername(username)
                .withPassword(password)
                .withLocation(location);

        long result = myDao.insertUser(userModel);

        return userModel.withId(String.valueOf(result));
    }

    public synchronized static RegisterServiceImpl get() {
        if (instance == null) {
            instance = new RegisterServiceImpl();
        }
        return instance;
    }

    public void closeDb(){
        myDao.close();
    }

    public void openDb(Context context){
        if(myDao == null){
            myDao = MyDao.get(context);
        }
        myDao.open();
    }
}
