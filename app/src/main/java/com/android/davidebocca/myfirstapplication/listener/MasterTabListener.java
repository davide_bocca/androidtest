package com.android.davidebocca.myfirstapplication.listener;



import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.android.davidebocca.myfirstapplication.R;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class MasterTabListener implements ActionBar.TabListener {

    private MediaPlayer mMediaPlayer;
    protected Context mContext;

    public MasterTabListener() {
        // Required empty public constructor
    }

    public MasterTabListener(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mMediaPlayer = MediaPlayer.create(mContext, R.raw.touchscreen);
        mMediaPlayer.start(); // no need to call prepare(); create() does that for you
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        mMediaPlayer.release();
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }


}
