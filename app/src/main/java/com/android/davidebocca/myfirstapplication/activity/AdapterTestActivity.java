package com.android.davidebocca.myfirstapplication.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.model.AdapterValueModel;
import com.android.davidebocca.myfirstapplication.service.impl.AdapterValueServiceImpl;
import com.android.davidebocca.myfirstapplication.util.Constants;

import java.util.LinkedList;
import java.util.List;

public class AdapterTestActivity extends Activity implements AbsListView.OnScrollListener {

    private ListView mCustomLayoutListView;
    private BaseAdapter mCustomLayoutAdapter;
    private int firstVisibleRow = 0;
    private int requestedPosition = 0;
    private TextView firstPositionVisible;
    private NumberPicker numberPicker;

    private final int RESULTS_NUMBER = 100;

    private List<AdapterValueModel> mModelList = new LinkedList<AdapterValueModel>();

    private final static String TAG_LOG = AdapterTestActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_test);
        firstPositionVisible = (TextView) findViewById(R.id.firstPositionVisibleText);
        numberPicker = (NumberPicker) findViewById(R.id.numberPicker);

        numberPicker.setMinValue(0);
        numberPicker.setMaxValue(RESULTS_NUMBER-1);

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                Log.d(TAG_LOG, "Changed value to -> " + picker.getValue());
                requestedPosition = picker.getValue();
            }
        });

        updatePositionText();

        manageAdapter();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        firstVisibleRow = firstVisibleItem;
        updatePositionText();
    }

    private void updatePositionText(){
        firstPositionVisible.setText(getString(R.string.first_position_visible,firstVisibleRow));
    }

    class Holder {
        TextView idTextView;
        TextView dateTextView;
        TextView valueTextView;
    }

    private void manageAdapter(){
        mCustomLayoutListView = (ListView) findViewById(R.id.listView);
        mCustomLayoutListView.setOnScrollListener(AdapterTestActivity.this);
        mCustomLayoutAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return mModelList.size();
            }

            @Override
            public Object getItem(int position) {
                return mModelList.get(position);
            }

            @Override
            public long getItemId(int position) {
                AdapterValueModel adapterValueModel = mModelList.get(position);
                return adapterValueModel.getmId();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                Holder holder;
                if(convertView == null){
                    convertView = getLayoutInflater().inflate(R.layout.custom_list_item, null);
                    holder = new Holder();
                    holder.idTextView = (TextView)
                            convertView.findViewById(R.id.id_element_text);
                    holder.dateTextView = (TextView)
                            convertView.findViewById(R.id.date_element_text);
                    holder.valueTextView = (TextView)
                            convertView.findViewById(R.id.value_element_text);
                    convertView.setTag(holder);
                }
                else {
                    holder = (Holder) convertView.getTag();
                }

                final AdapterValueModel adapterValueModel = (AdapterValueModel) getItem(position);

                holder.idTextView.setText(String.valueOf(adapterValueModel.getmId()));
                holder.dateTextView.setText(Constants.DATE_FORMAT.format(adapterValueModel.getmDate()));
                holder.valueTextView.setText(adapterValueModel.getmValue());

                return convertView;
            }
        };
        mCustomLayoutListView.setAdapter(mCustomLayoutAdapter);
    }

    @Override
    protected  void onStart(){
        super.onStart();
        final List<AdapterValueModel> myModels = AdapterValueServiceImpl.get().getValues(RESULTS_NUMBER);
        mModelList.clear();
        mModelList.addAll(myModels);

        mCustomLayoutAdapter.notifyDataSetChanged();

        mCustomLayoutListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Selected item: " + position,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void scrollTo(View v){

        final int durationPerItem = 40;
        final int offset = 0;
        final int fromHereToPosition = Math.abs(firstVisibleRow - requestedPosition);
        Log.d(TAG_LOG,"From visible to position -> " + fromHereToPosition + " rows");
        mCustomLayoutListView.smoothScrollToPositionFromTop(requestedPosition,offset,fromHereToPosition*durationPerItem);
    }

}
