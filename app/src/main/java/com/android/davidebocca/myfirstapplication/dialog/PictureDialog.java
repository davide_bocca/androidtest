package com.android.davidebocca.myfirstapplication.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.util.ImageUtils;

/**
 * Created by davide.bocca on 29/08/2014.
 */
public class PictureDialog extends DialogFragment implements
        android.view.View.OnClickListener  {

    private String mPath;

    public static PictureDialog newInstance(String path) {
        PictureDialog pictureDialog = new PictureDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("path", path);
        pictureDialog.setArguments(args);

        return pictureDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPath = getArguments().getString("path");
    }

    @Override
    public void onResume(){
        super.onResume();

//        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
//        params.width = 800;
//        params.height =  800;
//
//        getDialog().getWindow().setAttributes(params);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v = inflater.inflate(R.layout.picture_dialog, container);
        ImageView imageView = (ImageView) v.findViewById(R.id.image);
        ImageUtils.setPicInImageView(imageView,mPath);
        return v;
    }

//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        Dialog dialog = super.onCreateDialog(savedInstanceState);
//
//        // request a window without the title
////        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//
//        dialog.getWindow().setLayout(200, 200);
//        return dialog;
//    }

    @Override
    public void onClick(View v) {
        //o this.hide???
        this.dismiss();
    }

}
