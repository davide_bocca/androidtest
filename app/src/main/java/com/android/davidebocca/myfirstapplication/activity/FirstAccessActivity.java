package com.android.davidebocca.myfirstapplication.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.fragment.FirstAccessFragment;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.util.Constants;

public class FirstAccessActivity
        extends FragmentActivity
        implements FirstAccessFragment.FirstAccessListener {

    private final static String TAG_LOG = FirstAccessActivity.class.getName();

    private static SharedPreferences.Editor sharedPrefEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);
        if(savedInstanceState == null){
            final FirstAccessFragment firstAccessFragment = new FirstAccessFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.anchor_point, firstAccessFragment)
                    .commit();

            sharedPrefEditor = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE).edit();
            sharedPrefEditor.remove(getString(R.string.logged_username));
            sharedPrefEditor.commit();
        }
    }

    public void enterAsAnonymous(){
        final Intent anonymousIntent = new Intent(this, MainActivity.class);
        final UserModel userModel = UserModel.create(System.currentTimeMillis());
        anonymousIntent.putExtra(Constants.USER_MAIN_ACTIVITY_EXTRA, userModel);
        startActivity(anonymousIntent);
    }

    public void doLogin(){
        final Intent loginIntent = new Intent(Constants.LOGIN_ACTION);
        startActivityForResult(loginIntent,Constants.LOGIN_REQUEST_ID);
    }

    public void doRegistration(){
        final Intent registrationIntent = new Intent(Constants.REGISTRATION_ACTION);
        startActivityForResult(registrationIntent,Constants.REGISTRATION_REQUEST_ID);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == Constants.LOGIN_REQUEST_ID){
            switch (resultCode){
                case RESULT_OK:
                    final UserModel userModel = data.getParcelableExtra(Constants.USER_DATA_EXTRA);
                    final Intent mainIntent = new Intent(this, MainActivity.class);
                    mainIntent.putExtra(Constants.USER_MAIN_ACTIVITY_EXTRA,userModel);

                    sharedPrefEditor.putBoolean(getString(R.string.first_app_opening), true);
                    sharedPrefEditor.putString(getString(R.string.logged_username), userModel.getmUserName());
                    sharedPrefEditor.commit();

                    startActivity(mainIntent);
                    finish();
                break;
                case RESULT_CANCELED:
                break;
            }
        }
        else if(requestCode == Constants.REGISTRATION_REQUEST_ID){
            switch (resultCode){
                case RESULT_OK:
                    final UserModel userModel = data.getParcelableExtra(Constants.USER_DATA_EXTRA);
                    Log.d(TAG_LOG,"Usermodel id in activity -> " + userModel.getmId());
                    final Intent showUserData = new Intent(Constants.SHOW_USER_DATA_ACTION);
                    showUserData.putExtra(Constants.SHOW_USER_DATA_ACTION,userModel);

                    sharedPrefEditor.putBoolean(getString(R.string.first_app_opening), true);
                    sharedPrefEditor.putString(getString(R.string.logged_username), userModel.getmUserName());
                    sharedPrefEditor.commit();

                    startActivity(showUserData);
                    finish();
                break;
                case RESULT_CANCELED:
                    break;
            }
        }
    }
}
