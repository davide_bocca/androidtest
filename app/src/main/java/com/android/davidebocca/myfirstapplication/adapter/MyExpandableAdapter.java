package com.android.davidebocca.myfirstapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.model.AdapterValueModel;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.ContextUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by davide.bocca on 25/08/2014.
 */
public class MyExpandableAdapter extends BaseExpandableListAdapter {

    private List<AdapterValueModel> mModelList = new LinkedList<AdapterValueModel>();
    private Context mContext;

    public MyExpandableAdapter(List<AdapterValueModel> valueModelList, ContextUtils contextUtils){
        this.mModelList = valueModelList;
        this.mContext = contextUtils.mContext;
    }

    @Override
    public int getGroupCount() {
        return mModelList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        AdapterValueModel child = ((AdapterValueModel)getGroup(groupPosition));

        if(child.ismIsChild()){
            return child.getmChildrenNumber();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mModelList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mModelList.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return (mModelList.get(groupPosition)).getmId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return (mModelList.get(groupPosition)).getmId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        GroupHolder groupHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_list_group, null);

            groupHolder = new GroupHolder();
            groupHolder.groupIdTextView = (TextView) convertView.findViewById(R.id.listGroupItemId);
            convertView.setTag(R.integer.group_holder_tag_id, groupHolder);
        }
        else {
            groupHolder = (GroupHolder) convertView.getTag(R.integer.group_holder_tag_id);
        }

        final AdapterValueModel adapterValueModel = (AdapterValueModel) getGroup(groupPosition);

        groupHolder.groupIdTextView.setText(
                String.valueOf(adapterValueModel.getmId()) + " -> " + adapterValueModel.ismIsChild() + " - " + adapterValueModel.getmChildrenNumber());

//                if(getChildrenCount(groupPosition) == 0){
//                    mCustomExpandableListView.
//                }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View childConvertView, ViewGroup parent) {

        if(((AdapterValueModel)getGroup(groupPosition)).ismIsChild()){
            ChildHolder childHolder;
            if(childConvertView == null){
                childConvertView = LayoutInflater.from(mContext).inflate(R.layout.custom_list_item,null);
                childHolder = new ChildHolder();
                childHolder.idTextView = (TextView)
                        childConvertView.findViewById(R.id.id_element_text);
                childHolder.dateTextView = (TextView)
                        childConvertView.findViewById(R.id.date_element_text);
                childHolder.valueTextView = (TextView)
                        childConvertView.findViewById(R.id.value_element_text);
                childConvertView.setTag(R.integer.child_holder_tag_id, childHolder);
            }
            else {
                childHolder = (ChildHolder) childConvertView.getTag(R.integer.child_holder_tag_id);
            }

            final AdapterValueModel adapterValueModel = (AdapterValueModel) getChild(groupPosition,-1);

            childHolder.idTextView.setText(String.valueOf(childPosition));
            childHolder.dateTextView.setText(Constants.DATE_FORMAT.format(adapterValueModel.getmDate()));
            childHolder.valueTextView.setText(adapterValueModel.getmValue());
            return childConvertView;
        }
        return null;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class GroupHolder {
        TextView groupIdTextView;
    }

    class ChildHolder {
        TextView idTextView;
        TextView dateTextView;
        TextView valueTextView;
    }

}
