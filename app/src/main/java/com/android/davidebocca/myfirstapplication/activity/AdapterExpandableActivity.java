package com.android.davidebocca.myfirstapplication.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.adapter.MyExpandableAdapter;
import com.android.davidebocca.myfirstapplication.model.AdapterValueModel;
import com.android.davidebocca.myfirstapplication.service.impl.AdapterValueServiceImpl;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.ContextUtils;

import java.util.LinkedList;
import java.util.List;

public class AdapterExpandableActivity extends Activity {

    private ExpandableListView mCustomExpandableListView;
    private BaseExpandableListAdapter mCustomExpandableAdapter;

    private final int RESULTS_NUMBER = 50;

    private List<AdapterValueModel> mModelList = new LinkedList<AdapterValueModel>();

    private final static String TAG_LOG = AdapterExpandableActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_expandable);
        mCustomExpandableListView = (ExpandableListView) findViewById(R.id.custom_expandable_list_view);
        mCustomExpandableListView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                //now we can retrieve the width and height
                int width = mCustomExpandableListView.getWidth();

                mCustomExpandableListView.setIndicatorBoundsRelative(width-100,0);
                mCustomExpandableListView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        manageAdapter();
    }

    private void manageAdapter() {
        ContextUtils mContextUtils = new ContextUtils(getApplicationContext());
        mCustomExpandableAdapter = new MyExpandableAdapter(mModelList, mContextUtils);
        mCustomExpandableListView.setAdapter(mCustomExpandableAdapter);
    }

    @Override
    protected  void onStart(){
        super.onStart();
        final List<AdapterValueModel> myModels = AdapterValueServiceImpl.get().getValues(RESULTS_NUMBER);

        mModelList.clear();
        mModelList.addAll(myModels);

        mCustomExpandableAdapter.notifyDataSetChanged();
    }


}
