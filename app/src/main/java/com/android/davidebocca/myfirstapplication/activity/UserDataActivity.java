package com.android.davidebocca.myfirstapplication.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.util.Constants;

import org.w3c.dom.Text;

public class UserDataActivity extends MasterActivity {

    private TextView idText;
    private TextView usernameText;
    private TextView emailText;
    private TextView birthdateText;
    private TextView locationText;

    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);

        usernameText = (TextView) findViewById(R.id.username_text);
        idText = (TextView) findViewById(R.id.id_text);
        emailText = (TextView) findViewById(R.id.email_text);
        birthdateText = (TextView) findViewById(R.id.birthdate_text);
        locationText = (TextView) findViewById(R.id.location_text);


        Intent intent = getIntent();
        if(intent != null){
            userModel = intent.getParcelableExtra(Constants.SHOW_USER_DATA_ACTION);
            if(userModel != null){
                idText.setText(userModel.getmId());
                usernameText.setText(userModel.getmUserName());
                emailText.setText(userModel.getmEmail());
                birthdateText.setText(userModel.getBirthDate());
                locationText.setText(userModel.getmLocation());
            }
        }
    }

    public void goToMainActivity(View v){
        Intent mainIntent = new Intent(this, MainActivity.class);
        mainIntent.putExtra(Constants.USER_MAIN_ACTIVITY_EXTRA, userModel);
        startActivity(mainIntent);
    }
}
