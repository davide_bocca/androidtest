package com.android.davidebocca.myfirstapplication.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.model.AdapterValueModel;
import com.android.davidebocca.myfirstapplication.service.impl.AdapterValueServiceImpl;
import com.android.davidebocca.myfirstapplication.util.Constants;

import java.util.LinkedList;
import java.util.List;

public class AdapterCompareActivity extends Activity {

    private ListView mSimpleListView;
    private ListView mCustomItemListView;
    private ListView mCustomLayoutListView;

    private ArrayAdapter<AdapterValueModel> mSimpleAdapter;
    private ArrayAdapter<AdapterValueModel> mCustomItemAdapter;
    private BaseAdapter mCustomLayoutAdapter;

    private List<AdapterValueModel> mModelList = new LinkedList<AdapterValueModel>();

    private final static String TAG_LOG = AdapterCompareActivity.class.getName();

    class Holder {
        TextView idTextView;
        TextView dateTextView;
        TextView valueTextView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter_compare);
        showSimpleListAdapter();
        showCustomItemListAdapter();
        showCustomLayoutAdapter();
    }

    private void showSimpleListAdapter(){
        mSimpleListView = (ListView) findViewById(R.id.simpleListView);
        mSimpleAdapter = new ArrayAdapter<AdapterValueModel>(this,
                android.R.layout.simple_list_item_1,
                mModelList);
        mSimpleListView.setAdapter(mSimpleAdapter);
    }

    private void showCustomItemListAdapter(){
        mCustomItemListView = (ListView) findViewById(R.id.customItemListView);
        mCustomItemAdapter = new ArrayAdapter<AdapterValueModel>(this,
                R.layout.simple_custom_list_item,
                R.id.list_item_value,
                mModelList);
        mCustomItemListView.setAdapter(mCustomItemAdapter);
    }

    private void showCustomLayoutAdapter(){
        mCustomLayoutListView = (ListView) findViewById(R.id.customLayoutListView);
        mCustomLayoutAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return mModelList.size();
            }

            @Override
            public Object getItem(int position) {
                return mModelList.get(position);
            }

            @Override
            public long getItemId(int position) {
                AdapterValueModel adapterValueModel = mModelList.get(position);
                return adapterValueModel.getmId();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                Holder holder = null;
                if(convertView == null){
                    Log.d(TAG_LOG, "View -> null");
                    convertView = getLayoutInflater().inflate(R.layout.custom_list_item, null);
                    holder = new Holder();
                    holder.idTextView = (TextView)
                            convertView.findViewById(R.id.id_element_text);
                    holder.dateTextView = (TextView)
                            convertView.findViewById(R.id.date_element_text);
                    holder.valueTextView = (TextView)
                            convertView.findViewById(R.id.value_element_text);
                    convertView.setTag(holder);
                }
                else {
                    Log.d(TAG_LOG, "View -> NOT null");
                    holder = (Holder) convertView.getTag();
                }


                final AdapterValueModel adapterValueModel = (AdapterValueModel) getItem(position);

                holder.idTextView.setText(String.valueOf(adapterValueModel.getmId()));
                holder.dateTextView.setText(Constants.DATE_FORMAT.format(adapterValueModel.getmDate()));
                holder.valueTextView.setText(adapterValueModel.getmValue());

                return convertView;
            }
        };
        mCustomLayoutListView.setAdapter(mCustomLayoutAdapter);
    }

    @Override
    protected  void onStart(){
        super.onStart();
        final List<AdapterValueModel> myModels = AdapterValueServiceImpl.get().getValues(2);
        mModelList.clear();
        mModelList.addAll(myModels);

        mSimpleAdapter.notifyDataSetChanged();
        mCustomItemAdapter.notifyDataSetChanged();
        mCustomLayoutAdapter.notifyDataSetChanged();
    }
}
