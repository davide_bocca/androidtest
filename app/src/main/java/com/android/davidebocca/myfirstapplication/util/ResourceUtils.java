package com.android.davidebocca.myfirstapplication.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by davide.bocca on 25/08/2014.
 */
public class ResourceUtils {

    private final static String TAG_LOG = ResourceUtils.class.getName();

    // convert raw resource to String
    public static String getStringFromRawResource(Context context, int resId) throws IOException {

        InputStream inputStream = context.getResources().openRawResource(resId);

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }

        inputStream.close();

        return sb.toString();

    }

    public static String getPropertyFromRawPropertiesFile(int resId, String property, Context context) {
        try{
            InputStream rawResource = context.getResources().openRawResource(resId);
            Properties properties = new Properties();

            properties.load(rawResource);
            if(properties != null){
                return properties.getProperty(property);
            }

        } catch (IOException e) {
            Log.e(TAG_LOG, "Error getting properties file",e);
        }

        return null;
    }

}
