package com.android.davidebocca.myfirstapplication.fragment;



import android.app.Fragment;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.MainActivity;
import com.android.davidebocca.myfirstapplication.util.Constants;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class BimbiFragment extends MasterFragment {

    private static final String TAG_LOG = BimbiFragment.class.getName();

    public BimbiFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_bimbi, null, false);
        return root;
    }

    public void gestisciClickBimbo(View v){
        String tag = v.getTag().toString();
        String message = "";

        if(image != null && image.getAnimation() != null){
            image.setAnimation(null);
        }

        if(tag.equals("nico")){
            message = getResources().getString(R.string.frase_nico);
        }
        else if(tag.equals("dada")) {
            message = getResources().getString(R.string.frase_dadà);
        }

        ((MainActivity) getActivity()).makeToastWithMessage(message);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, getActivity().getApplicationContext().getString(R.string.BIMBIUTTERANCEID));

        speak(message, params, false);

        image=(ImageView)getActivity().findViewById(v.getId());

        manageAnimation();
    }

    private void manageAnimation(){

        Animation anim = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.bimbi_animation);
        image.startAnimation(anim);

        if(!Constants.MUTE_APP){
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    image.setAnimation(null);
                }
            }, 3000);
        }
    }

}
