package com.android.davidebocca.myfirstapplication.fragment;



import android.os.Bundle;
import android.app.Fragment;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.MainActivity;
import com.android.davidebocca.myfirstapplication.util.Constants;

import java.util.HashMap;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class MasterFragment extends Fragment {

    private static final String TAG_LOG = MasterFragment.class.getName();
    private TextToSpeech textToSpeech = null;
    ImageView image = null;

    public MasterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initializeTextToSpeech();
        return null;
    }

    private void initializeTextToSpeech() {
        textToSpeech = new TextToSpeech(getActivity().getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {

            }
        });

        textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String s) {

            }

            @Override
            public void onDone(String s) {
                if (s.equals(getActivity().getApplicationContext().getString(R.string.BIMBIUTTERANCEID))) {
                    if (image != null && image.getAnimation() != null) {
                        image.getAnimation().setFillAfter(true);
                        image.setAnimation(null);
                    }
                }
                Log.v(TAG_LOG, "Speak finito");
            }

            @Override
            public void onError(String s) {

            }
        });
    }

    public void speak(String message, HashMap<String, String> params, boolean ignoreMute){
        Log.d(TAG_LOG, "MUTE_APP: " + Constants.MUTE_APP + " - " + "ignoreMute: " + ignoreMute);
        if(!Constants.MUTE_APP || ignoreMute){
            Log.d(TAG_LOG, "Speaking: " + message);
            Locale selectedLocale = ((MainActivity) getActivity()).getTextToSpeechLocale();

            if(textToSpeech.getLanguage() != selectedLocale){
                textToSpeech.setLanguage(selectedLocale);
            }
            textToSpeech.speak(message,TextToSpeech.QUEUE_FLUSH,params);
        }
    }

}
