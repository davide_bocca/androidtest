package com.android.davidebocca.myfirstapplication.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.exception.MyException;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.service.impl.RegisterServiceImpl;
import com.android.davidebocca.myfirstapplication.util.Constants;

import java.util.Calendar;

public class RegisterActivity extends Activity {

    private final static String TAG_LOG = RegisterActivity.class.getName();

    private EditText mUsername;
    private EditText mPassword;
    private EditText mEmail;
    private DatePicker mBirthdate;
    private EditText mLocation;
    private TextView mErrorRegistration;
    private Calendar birthdateCal;


    private RegisterServiceImpl mRegisterServiceImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mUsername = (EditText) findViewById(R.id.username_edittext);
        mPassword = (EditText) findViewById(R.id.password_edittext);
        mEmail = (EditText) findViewById(R.id.email_edittext);
        mBirthdate = (DatePicker) findViewById(R.id.birthdate_value);
        mLocation = (EditText) findViewById(R.id.location_edittext);

        mErrorRegistration = (TextView) findViewById(R.id.error_message);

        birthdateCal = Calendar.getInstance();

        birthdateCal.set(Calendar.YEAR, birthdateCal.get(Calendar.YEAR)-18);

        mBirthdate.setMaxDate(birthdateCal.getTimeInMillis());

        mBirthdate.init(birthdateCal.get(Calendar.YEAR),
                birthdateCal.get(Calendar.MONTH),
                birthdateCal.get(Calendar.DAY_OF_MONTH),
                new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker datePicker, int year, int month, int day) {
                        birthdateCal.set(year, month, day);
                    }
                });
    }

    @Override
    public void onStop(){
        if(mRegisterServiceImpl != null){
            mRegisterServiceImpl.closeDb();
        }
        super.onStop();
    }

    public void doRegistration(View v){

        mErrorRegistration.setVisibility(View.INVISIBLE);

        final Editable userName = mUsername.getText();
        if(TextUtils.isEmpty(userName)){
            showError("username");
            return;
        }

        final Editable password = mPassword.getText();
        if(TextUtils.isEmpty(password)){
            showError("password");
            return;
        }

        final Editable email = mEmail.getText();
        if(TextUtils.isEmpty(email)){
            showError("email");
            return;
        }

        final Editable location = mLocation.getText();
        if(TextUtils.isEmpty(location)){
            showError("località");
            return;
        }

        final String usernameText = mUsername.getText().toString();
        final String passwordText = mPassword.getText().toString();
        final String emailText = mEmail.getText().toString();

        final String locationText = mLocation.getText().toString();
        mRegisterServiceImpl = RegisterServiceImpl.get();
        mRegisterServiceImpl.openDb(getApplicationContext());
        final UserModel userModel;
        try {
            userModel = mRegisterServiceImpl.register(usernameText, passwordText, emailText, birthdateCal.getTimeInMillis(), locationText);
        } catch (MyException e) {
            this.mErrorRegistration.setText(getResources().getString(R.string.create_user_error));
            this.mErrorRegistration.setVisibility(View.VISIBLE);
            return;
        }

        Log.d(TAG_LOG,"User not null, ID -> " + userModel.getmId());
        Toast.makeText(getApplicationContext(),"ID User -> " + userModel.getmId(),Toast.LENGTH_LONG).show();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.USER_DATA_EXTRA,userModel);
        setResult(RESULT_OK,resultIntent);
        finish();
    }

    private void showError(String field){
        mErrorRegistration.setText(getResources().getString(R.string.mandatory_field_error,field));
        mErrorRegistration.setVisibility(View.VISIBLE);
    }
}
