package com.android.davidebocca.myfirstapplication.service.impl;

import com.android.davidebocca.myfirstapplication.model.AdapterValueModel;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.service.AdapterValueService;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by davide.bocca on 19/08/2014.
 */
public class AdapterValueServiceImpl implements AdapterValueService {

    private static AdapterValueServiceImpl instance;
    private static long startTime;
    private static long endTime;

    @Override
    public List<AdapterValueModel> getValues(int customNumber) {

        List<AdapterValueModel> values = new LinkedList<AdapterValueModel>();

        for(int i=0;i<customNumber; i++){
            AdapterValueModel value = AdapterValueModel
                    .create(i, getRandomTimeBetweenTwoDates(), "Value 1", Math.random() < 0.5);

            if(value.ismIsChild()){
                value.withChildren(new Random().nextInt(6)+1);
            }

            values.add(value);
        }

        return values;
    }

    public synchronized static AdapterValueServiceImpl get(){
        if(instance == null){
            instance = new AdapterValueServiceImpl();
            startTime = Timestamp.valueOf("1981-01-26 01:00:00").getTime();
            endTime = Timestamp.valueOf("2014-01-26 01:00:00").getTime();
        }

        return instance;
    }

    private long getRandomTimeBetweenTwoDates () {
        long diff = endTime - startTime + 1;
        return startTime + (long) (Math.random() * diff);
    }

}
