package com.android.davidebocca.myfirstapplication.listener;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.media.MediaPlayer;

import com.android.davidebocca.myfirstapplication.R;

/**
 * Created by davide.bocca on 27/08/2014.
 */
public class FragmentRemoveTabListener extends MasterTabListener {

    private Fragment mFragment;

    public FragmentRemoveTabListener(Fragment fragment, Context context){
        super(context);
        this.mFragment = fragment;
        mContext = context;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        super.onTabSelected(tab,ft);
        ft.add(R.id.anchor_container,mFragment);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        super.onTabUnselected(tab,ft);
        ft.remove(mFragment);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        super.onTabReselected(tab,ft);
    }
}
