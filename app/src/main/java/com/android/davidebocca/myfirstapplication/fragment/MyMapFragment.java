package com.android.davidebocca.myfirstapplication.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.MainActivity;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MyMapFragment extends MasterFragment {

    private final static String TAG_LOG = MyMapFragment.class.getName();
    private final static LatLng CASA = new LatLng(44.861975, 8.741674);

    private GoogleMap map = null;
    private MapFragment mapFragment = null;
    private View root = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        if(root == null){
            root = inflater.inflate(R.layout.fragment_map, null, false);
            initializeMap();
        }
        return root;
    }

    private void manageMapOpening(){

        setPadding(map);

        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(CASA, 1));

        map.addMarker(new MarkerOptions()
                .title("Casa")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.translate_active))
                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                .position(CASA));
    }

    private void setPadding(GoogleMap map){
        map.setPadding(0,10,0,0);
    }

    private void manageMapEvents(){
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                String message = getAddressString(latLng);
                speak(message, null,false);
                ((MainActivity) getActivity()).makeToastWithMessage(message);
            }
        });
    }

    private void initializeMap()
    {
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mapFragment = MapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mapFragment).commit();
        }
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        if(map != null){
            manageMapOpening();
            manageMapEvents();
        }
    }

    private String getAddressString(LatLng latLng){
        Geocoder geocoder;
        List<Address> addresses;

        String address = "";
        String city = "";
        String country = "";

        String message;
        geocoder = new Geocoder(getActivity().getApplicationContext(), ((MainActivity) getActivity()).getTextToSpeechLocale());

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG_LOG, "Error getting address");
            return "Errore durante la ricerca dell'indirizzo";
        }

        if(addresses.size() > 0){
            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);
            country = addresses.get(0).getAddressLine(2);

            message = ((address == null) ? "" : address + ", " ) +
                    ((city == null) ? "" : city + ", ") +
                    ((country == null) ? "" : country);

            if(message.endsWith(", ")){
                message = message.replace(", ", "");
            }

        }
        else {
            message = "Non è stato possibile individuare un indirizzo valido";
        }

        return message;
    }
}