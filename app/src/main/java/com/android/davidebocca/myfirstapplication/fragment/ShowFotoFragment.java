package com.android.davidebocca.myfirstapplication.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.TabNavigationActivity;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.ImageUtils;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class ShowFotoFragment extends Fragment {

    public final static String TAG_LOG = ShowFotoFragment.class.getName();

    private ImageView mImageView;
    private String takenImagePath;
    private Button mDeletePhoto;

    public ShowFotoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG_LOG,"Saved instance != null -> " + (savedInstanceState != null));
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_show_photo, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        mImageView = (ImageView) getActivity().findViewById(R.id.taken_image);
        mDeletePhoto = (Button) getActivity().findViewById(R.id.delete_photo_button);
        if(takenImagePath != null){
            ImageUtils.setPicInImageView(mImageView, takenImagePath);
        }
        mDeletePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageUtils.deleteImageAndRemovePreview(mImageView,takenImagePath,new WeakReference<Activity>(getActivity()));
            }
        });
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        takenImagePath = ((TabNavigationActivity) activity).getTakenImagePath();
    }

    public static ShowFotoFragment getInstance(){
        ShowFotoFragment showFotoFragment = new ShowFotoFragment();
        return showFotoFragment;
    }

}
