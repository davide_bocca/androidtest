package com.android.davidebocca.myfirstapplication.activity;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.SharedPrefUtils;
import com.android.davidebocca.myfirstapplication.util.SystemUiHider;

import java.lang.ref.WeakReference;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class SplashScreenActivity extends Activity {
    /**
     * The Tag for the Log
     */
//    private static final String TAG_LOG = SplashScreen.class.getName();
    private static final String TAG_LOG = "LIFECYCLE";

    /**
     * The minimum interval we have to wait before going ahead
     */
    private static final long MIN_WAIT_INTERVAL = 1500L;

    /**
     * The maximum interval we can wait before going ahead
     */
    private static final long MAX_WAIT_INTERVAL = 3000L;

    /**
     * The What to use into the Handler to go to the next Activity
     */
    private static final int GO_AHEAD_WHAT = 1;

    /**
     * The time we consider as the start
     */
    private long mStartTime;

    /**
     * This variable is used to prevent the double launch of the next activity
     * or the launch when the current Activity has been closed.
     */
    private boolean mIsDone;

    /**
     * This is a static class that doesn't retain the reference to the container class thought
     * the this reference. The reference to the container Activity is make through the
     * WeakReference that is not counted as a valid reference by the GC
     */
    private static class UiHandler extends Handler {

        /**
         * The WeakReference to the Activity that uses it
         */
        private WeakReference<SplashScreenActivity> mActivityRef;

        /**
         * Constructor with the sourceActivity reference
         *
         * @param srcActivity The Activity we need a reference of.
         */
        public UiHandler(final SplashScreenActivity srcActivity) {
            // We just save the reference to the src Activity
            this.mActivityRef = new WeakReference<SplashScreenActivity>(srcActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            // We get the reference to the sourceActivity
            final SplashScreenActivity srcActivity = this.mActivityRef.get();
            if (srcActivity == null) {
                // In this case the reference to the Activity is lost. It should not be
                // the case.
                Log.d(TAG_LOG, "Reference to NoLeakSplashActivity lost!");
                return;
            }
            switch (msg.what) {
                case GO_AHEAD_WHAT:
                    // We calculate le elapsed time to prevent the early launch of the next
                    // Activity
                    long elapsedTime = SystemClock.uptimeMillis() - srcActivity.mStartTime;
                    // We go ahead if not closed. We could use isDestroyed() but is available only
                    // from API Level 17. Then we need to use an instance variable.
                    if (elapsedTime >= MIN_WAIT_INTERVAL && !srcActivity.mIsDone) {
                        srcActivity.mIsDone = true;
                        srcActivity.goAhead();
                    }
                    break;
            }
        }

    }

    /**
     * This is the Handler we use to manage time interval
     */
    private UiHandler mHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG_LOG,"SplashScreen -> ON CREATE");
        super.onCreate(savedInstanceState);
        boolean alreadyOpened = SharedPrefUtils.get(getApplicationContext()).alreadyOpenApp();

        if(alreadyOpened){
            goAhead();
            this.finish();
        }

        setContentView(R.layout.activity_splash_screen);
        if(!Constants.MUTE_APP) {
            MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.paperino);
            mediaPlayer.start(); // no need to call prepare(); create() does that for you
        }
        // We initialize the Handler
        mHandler = new UiHandler(this);
    }

    @Override
    protected void onStart() {
        Log.d(TAG_LOG,"SplashScreen -> ON START");
        super.onStart();
        // We set the time to consider as the start
        mStartTime = SystemClock.uptimeMillis();
        // We set the time for going ahead automatically
        final Message goAheadMessage = mHandler.obtainMessage(GO_AHEAD_WHAT);
        mHandler.sendMessageAtTime(goAheadMessage, mStartTime + MAX_WAIT_INTERVAL);
//        Log.d(TAG_LOG, "Handler message sent!");
    }

    @Override
    protected void onResume(){
        Log.d(TAG_LOG,"SplashScreen -> ON RESUME");
        super.onResume();
    }

    @Override
    protected void onPause(){
        Log.d(TAG_LOG,"SplashScreen -> ON PAUSE");
        super.onPause();
    }

    @Override
    protected void onStop(){
        Log.d(TAG_LOG,"SplashScreen -> ON STOP");
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        Log.d(TAG_LOG,"SplashScreen -> ON DESTROY");
        super.onDestroy();
    }

    /**
     * Utility method that manages the transition to the FirstAccessActivity
     */
    private void goAhead() {
        // We create the explicit Intent
        final Intent intent = new Intent(this, FirstAccessActivity.class);
        // Launch the Intent
        startActivity(intent);
        // We finish the current Activity
        finish();
    }
}
