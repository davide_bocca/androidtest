package com.android.davidebocca.myfirstapplication.model;

/**
 * Created by davide.bocca on 20/08/2014.
 */
public class AdapterValueModel {

    private long mId;
    private long mDate;
    private String mValue;
    private boolean mIsChild;
    private int mChildrenNumber;

    private AdapterValueModel(long id, long date, String value, boolean isChild){
        mId = id;
        mDate = date;
        mValue = value;
        mIsChild = isChild;
    }

    public static AdapterValueModel create(final long id, long date, String value, boolean isChild){
        return new AdapterValueModel(id, date, value, isChild);
    }

    public AdapterValueModel withChildren(int childrenNumber){
        mChildrenNumber = childrenNumber;
        return this;
    }

    public long getmId() {
        return mId;
    }

    public long getmDate() {
        return mDate;
    }

    public String getmValue() {
        return mValue;
    }

    public int getmChildrenNumber() {
        return mChildrenNumber;
    }

    public boolean ismIsChild() {
        return mIsChild;
    }
}
