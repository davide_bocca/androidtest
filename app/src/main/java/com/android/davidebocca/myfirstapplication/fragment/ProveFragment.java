package com.android.davidebocca.myfirstapplication.fragment;



import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.AdapterCompareActivity;
import com.android.davidebocca.myfirstapplication.activity.AdapterExpandableActivity;
import com.android.davidebocca.myfirstapplication.activity.AdapterTestActivity;
import com.android.davidebocca.myfirstapplication.activity.LayoutTestActivity;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ProveFragment extends MasterFragment {

    private static final String TAG_LOG = ProveFragment.class.getName();

    private EditText urlText;
    private TextView errorMessage;
    private Button openUrlButton;
    private Button openLayoutTestButton;
    private Button openAdapterCompareButton;
    private Button openAdapterTestButton;
    private Button openAdapterExpandableButton;
    private TextView title;

    private int counter = 0;

    public ProveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_prove, null, false);

        urlText = (EditText) root.findViewById(R.id.url_text);
        errorMessage = (TextView) root.findViewById(R.id.error_message);
        openUrlButton = (Button) root.findViewById(R.id.open_url_button);
        openLayoutTestButton = (Button) root.findViewById(R.id.open_layout_test_button);
        openAdapterCompareButton = (Button) root.findViewById(R.id.open_adapter_compare_button);
        openAdapterTestButton = (Button) root.findViewById(R.id.open_adapter_test_button);
        openAdapterExpandableButton = (Button) root.findViewById(R.id.open_adapter_expandable_button);

        openUrlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUrl();
            }
        });

        openLayoutTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LayoutTestActivity.class);
                startActivity(intent);
            }
        });

        openAdapterCompareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AdapterCompareActivity.class);
                startActivity(intent);
            }
        });

        openAdapterTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AdapterTestActivity.class);
                startActivity(intent);
            }
        });

        openAdapterExpandableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AdapterExpandableActivity.class);
                startActivity(intent);
            }
        });

        title = (TextView) root.findViewById(R.id.firstActivityTitle);

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;

                if(counter == 10){
                    speak("Ho la figa nel culo",null,true);
                }
            }
        });

        populateRadioButtonString(root);
        return root;
    }

    public void populateRadioButtonString(View root){
        RadioGroup myRadioGroup = (RadioGroup) root.findViewById(R.id.radioButtons);

        for (int i = 0; i < myRadioGroup.getChildCount(); i++) {
            RadioButton actualRadioButton = (RadioButton) myRadioGroup.getChildAt(i);
            String newText = String.format(actualRadioButton.getText().toString(), i+1);
            actualRadioButton.setText(newText);
        }
    }

    public void openUrl(){

        errorMessage.setVisibility(View.INVISIBLE);

        final Editable url = urlText.getText();

        if(TextUtils.isEmpty(url)){
            errorMessage.setText(R.string.error_empty_url);
            errorMessage.setVisibility(View.VISIBLE);
            urlText.setText(getString(R.string.url_prefix));
            return;
        }

        if(!URLUtil.isValidUrl(url.toString())){
            errorMessage.setText(R.string.error_valid_url);
            errorMessage.setVisibility(View.VISIBLE);
            urlText.setText(getString(R.string.url_prefix));
            return;
        }

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url.toString()));

        startActivity(i);
    }
}
