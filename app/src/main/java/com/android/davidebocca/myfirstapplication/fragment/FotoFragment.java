package com.android.davidebocca.myfirstapplication.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.TabNavigationActivity;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.ImageUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class FotoFragment extends Fragment {

    private ImageButton mImageButton;
    private String mCurrentPhotoPath;

    private final static String TAG_LOG = FotoFragment.class.getName();

    public FotoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_foto, container, false);
    }

    public void onStart(){
        super.onStart();
        // Inflate the layout for this fragment
        mImageButton = (ImageButton)getActivity().findViewById(R.id.photo_button);
        mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture();
            }
        });
    }

    private void takePicture(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = ImageUtils.createImageFile();
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                Log.e(TAG_LOG,"Error creating image file",ex);
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Log.d(TAG_LOG, "Selected file name -> " + photoFile.getName());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, Constants.TAKE_PICTURE_REQUEST_ID);
            }
            else {
                Toast.makeText(getActivity().getApplicationContext(),"Unable to create the file",Toast.LENGTH_LONG);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
//        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == Constants.TAKE_PICTURE_REQUEST_ID && resultCode == getActivity().RESULT_OK){
            ImageUtils.mediaGalleryScan(mCurrentPhotoPath, new WeakReference<Activity>(getActivity()));
            Intent intent = new Intent(getActivity().getApplicationContext(), TabNavigationActivity.class);
            intent.putExtra(Constants.TAKEN_IMAGE_PATH_EXTRA, mCurrentPhotoPath);
            startActivity(intent);
        }
    }


}
