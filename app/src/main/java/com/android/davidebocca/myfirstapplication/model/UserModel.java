package com.android.davidebocca.myfirstapplication.model;

import android.text.TextUtils;

/**
 * Created by davide.bocca on 11/08/2014.
 */
public class UserModel {

    private long birthDate;
    private String mUserName;
    private String mPassword;
    private String mEmail;
    private String mLocation;

    private UserModel(long birthDate) {
        this.birthDate = birthDate;
    }

    public static UserModel create(final long birthDate){
        final UserModel userModel = new UserModel(birthDate);
        return userModel;
    }

    public UserModel withEmail(String email){
        if (email == null) {
            throw new IllegalArgumentException("Email cannot be null here!");
        }
        mEmail = email;
        return this;
    }

    public String getmUserName() {
        return mUserName;
    }

    public boolean isAnonymous(){
        return TextUtils.isEmpty(mUserName);
    }

    public boolean isLogged(){
        return !TextUtils.isEmpty(mUserName);
    }
}
