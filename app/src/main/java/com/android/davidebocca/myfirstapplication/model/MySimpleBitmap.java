package com.android.davidebocca.myfirstapplication.model;

import android.graphics.Bitmap;

/**
 * Created by davide.bocca on 29/08/2014.
 */
public class MySimpleBitmap {

    private Bitmap mBitmap;
    private String mPath;

    public MySimpleBitmap(Bitmap mBitmap, String mPath) {
        this.mBitmap = mBitmap;
        this.mPath = mPath;
    }

    public static MySimpleBitmap get(Bitmap mBitmap, String mPath){
        return new MySimpleBitmap(mBitmap,mPath);
    }

    public Bitmap getmBitmap() {
        return mBitmap;
    }

    public String getmPath() {
        return mPath;
    }
}
