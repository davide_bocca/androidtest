package com.android.davidebocca.myfirstapplication.activity;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.fragment.BimbiFragment;
import com.android.davidebocca.myfirstapplication.fragment.FotoFragment;
import com.android.davidebocca.myfirstapplication.fragment.MyMapFragment;
import com.android.davidebocca.myfirstapplication.fragment.NetworkFragment;
import com.android.davidebocca.myfirstapplication.fragment.ProveFragment;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.util.Constants;
import com.android.davidebocca.myfirstapplication.util.SharedPrefUtils;

import org.w3c.dom.Text;

public class MainActivity extends MasterActivity {

    private DrawerLayout drawerLayout;
    private ListView leftMenu;
    private String[] menuValues;

    private static final String TAG_LOG = MainActivity.class.getName();
    private int actualPosition = -1;

    private int counter = 0;

    private BimbiFragment bimbiFragment = null;
    private ProveFragment proveFragment = null;
    private MyMapFragment myMapFragment = null;
    private FotoFragment fotoFragment = null;
    private NetworkFragment networkFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Intent intent = getIntent();
//        if(intent != null){
//            UserModel userModel = intent.getParcelableExtra(Constants.USER_MAIN_ACTIVITY_EXTRA);
//            if(userModel != null && userModel.isLogged()){
//                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
//                alertDialog.setTitle(getResources().getString(R.string.welcome_dialog_title));
//                alertDialog.setMessage(getResources().getString(R.string.welcome_dialog_text, userModel.getmUserName()));
//                alertDialog.setIcon(R.drawable.ic_action_accept);
//                alertDialog.show();
//            }
//        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        leftMenu = (ListView) findViewById(R.id.left_drawer);
        menuValues = getResources().getStringArray(R.array.menu_values);
        leftMenu.setOnItemClickListener(new DrawerItemClickListener());

//        leftMenu.setAdapter(new ArrayAdapter<String>(this,R.layout.menu_list_item, R.id.text_menu_list, menuValues));
        leftMenu.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return menuValues.length;
            }

            @Override
            public Object getItem(int position) {
                return menuValues[position];
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ItemHolder itemHolder;
                if(convertView == null ){
                    convertView = getLayoutInflater().inflate(R.layout.left_menu_item,null);
                    itemHolder = new ItemHolder();
                    itemHolder.menuItemText = (TextView) convertView.findViewById(R.id.left_menu_item_text);
                    convertView.setTag(itemHolder);
                }
                else {
                    itemHolder = (ItemHolder) convertView.getTag();
                }

                int resId = -1;

                switch (position){
                    case 3:
                        if(SharedPrefUtils.get(getApplicationContext()).isUserLogged()){
                            resId = R.drawable.ic_action_camera;
                        }
                        else {
                            convertView.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 4:
                        if(SharedPrefUtils.get(getApplicationContext()).isUserLogged()){
                            resId = R.drawable.ic_action_network_cell;
                        }
                        else {
                            convertView.setVisibility(View.INVISIBLE);
                        }
                        break;
                    default:
                        resId = R.drawable.ic_action_cloud;
                    break;
                }
                if(resId != -1) {
                    itemHolder.menuItemText.setCompoundDrawablesWithIntrinsicBounds(resId, 0, 0, 0);
                }
                itemHolder.menuItemText.setText((String)getItem(position));
                return convertView;
            }
        });

        // set a custom shadow that overlays the main content when the drawer opens
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        drawerToggle = new ActionBarDrawerToggle(
                MainActivity.this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        selectItem(4);
    }

    class ItemHolder {
        TextView menuItemText;
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        // update selected item and title, then close the drawer

        FragmentManager fragmentManager = getFragmentManager();

        if(actualPosition != position){
            actualPosition = position;
            switch (position){
                case 0:
                    if(proveFragment == null){
                        proveFragment = new ProveFragment();
                    }
                    setActiveFragment(getApplicationContext().getString(R.string.ACTUAL_PROVE));
                    fragmentManager.beginTransaction().replace(R.id.content_frame, proveFragment).commit();
                break;
                case 1:
                    if(myMapFragment == null){
                        myMapFragment = new MyMapFragment();
                    }
                    setActiveFragment(getApplicationContext().getString(R.string.ACTUAL_MAP));
                    fragmentManager.beginTransaction().replace(R.id.content_frame, myMapFragment).commit();
                break;
                case 2:
                    if(bimbiFragment == null){
                        bimbiFragment = new BimbiFragment();
                    }
                    setActiveFragment(getApplicationContext().getString(R.string.ACTUAL_BIMBI));
                    fragmentManager.beginTransaction().replace(R.id.content_frame, bimbiFragment).commit();
                break;
                case 3:
                    if(fotoFragment == null){
                        fotoFragment = new FotoFragment();
                    }
                    setActiveFragment(getApplicationContext().getString(R.string.ACTUAL_FOTO));
                    fragmentManager.beginTransaction().replace(R.id.content_frame, fotoFragment).commit();
                break;
                case 4:
                    if(networkFragment == null){
                        networkFragment = new NetworkFragment();
                    }
                    setActiveFragment(getApplicationContext().getString(R.string.ACTUAL_NETWORK));
                    fragmentManager.beginTransaction().replace(R.id.content_frame, networkFragment).commit();
                    break;
            }
            leftMenu.setItemChecked(position, true);
            setTitle(menuValues[position]);
        }

        drawerLayout.closeDrawer(leftMenu);

    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void gestisciClickBimbo(View v){
        bimbiFragment.gestisciClickBimbo(v);
    }

}