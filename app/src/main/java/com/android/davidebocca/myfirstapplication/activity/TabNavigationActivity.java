package com.android.davidebocca.myfirstapplication.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.fragment.GalleryFragment;
import com.android.davidebocca.myfirstapplication.fragment.ShowFotoFragment;
import com.android.davidebocca.myfirstapplication.listener.FragmentRemoveTabListener;
import com.android.davidebocca.myfirstapplication.listener.NoFragmentRemoveTabListener;
import com.android.davidebocca.myfirstapplication.util.Constants;

public class TabNavigationActivity extends Activity {

    private String takenImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();

        takenImagePath = intent.getStringExtra(Constants.TAKEN_IMAGE_PATH_EXTRA);

        setContentView(R.layout.activity_tab_navigation);
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.Tab tab1 = actionBar.newTab().setText("Foto");
        ActionBar.Tab tab2 = actionBar.newTab().setText("Galleria");
//        tab1.setIcon(R.drawable.ic_action_camera);
//        tab2.setIcon(R.drawable.ic_action_collection);
        ShowFotoFragment fragment1 = ShowFotoFragment.getInstance();
        GalleryFragment fragment2 = GalleryFragment.getInstance();
        tab1.setTabListener(new NoFragmentRemoveTabListener(fragment1,getApplicationContext()));
        tab2.setTabListener(new NoFragmentRemoveTabListener(fragment2,getApplicationContext()));
        actionBar.addTab(tab1);
        actionBar.addTab(tab2);
        int selectedTabIndex = 0;
        if(savedInstanceState != null){
            selectedTabIndex = savedInstanceState.getInt(Constants.SELECTED_TAB_INDEX_PARAM);
        }
        actionBar.setSelectedNavigationItem(selectedTabIndex);
    }

    @Override
    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        int selectedTabIndex = getActionBar().getSelectedNavigationIndex();
        outState.putInt(Constants.SELECTED_TAB_INDEX_PARAM,selectedTabIndex);
    }

    public String getTakenImagePath() {
        return takenImagePath;
    }
}
