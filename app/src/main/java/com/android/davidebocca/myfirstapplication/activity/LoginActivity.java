package com.android.davidebocca.myfirstapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.model.par.UserModel;
import com.android.davidebocca.myfirstapplication.service.impl.LoginServiceImpl;
import com.android.davidebocca.myfirstapplication.util.Constants;

public class LoginActivity extends MasterActivity {

    private static TextView mErrorTextView;
    private static EditText mUsernameEditText;
    private static EditText mPasswordEditText;

    private LoginServiceImpl mLoginService;

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_login);
        mErrorTextView = (TextView) findViewById(R.id.error_message);
        mUsernameEditText = (EditText) findViewById(R.id.username_edittext);
        mPasswordEditText = (EditText) findViewById(R.id.password_edittext);
    }

    @Override
    public void onStop(){
        if(mLoginService != null){
            mLoginService.closeDb();
        }
        super.onStop();
    }

   public void doLogin(View v){

       if(mLoginService == null){
           mLoginService = LoginServiceImpl.get();
       }
       mLoginService.openDb(getApplicationContext());

       this.mErrorTextView.setVisibility(View.INVISIBLE);
       final Editable mUsernameEdit = mUsernameEditText.getText();
       if(TextUtils.isEmpty(mUsernameEdit)){
           final String usernameMandatory = getResources().getString(R.string.mandatory_field_error, "username");
           this.mErrorTextView.setText(usernameMandatory);
           this.mErrorTextView.setVisibility(View.VISIBLE);
           return;
       }

       final Editable mPasswordEdit = mPasswordEditText.getText();
       if(TextUtils.isEmpty(mPasswordEdit)){
           final String passwordMandatory = getResources().getString(R.string.mandatory_field_error, "password");
           this.mErrorTextView.setText(passwordMandatory);
           this.mErrorTextView.setVisibility(View.VISIBLE);
           return;
       }

       final String username = mUsernameEdit.toString();
       final String password = mPasswordEdit.toString();
       final UserModel userModel = LoginServiceImpl.get().login(username,password,getApplicationContext());
       if(userModel != null){
           Intent resultIntent = new Intent();
           resultIntent.putExtra(Constants.USER_DATA_EXTRA,userModel);
           setResult(RESULT_OK,resultIntent);
           finish();
       }
       else {
           this.mErrorTextView.setText(getResources().getString(R.string.wrong_credential_error));
           this.mErrorTextView.setVisibility(View.VISIBLE);
       }
   }
}
