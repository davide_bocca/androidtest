package com.android.davidebocca.myfirstapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.davidebocca.myfirstapplication.R;
import com.android.davidebocca.myfirstapplication.activity.TabNavigationActivity;
import com.android.davidebocca.myfirstapplication.dialog.PictureDialog;
import com.android.davidebocca.myfirstapplication.model.MySimpleBitmap;
import com.android.davidebocca.myfirstapplication.util.ImageUtils;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by davide.bocca on 28/08/2014.
 */
public class GalleryImageAdapter extends BaseAdapter {

    private List<String> mImagePath;
    private WeakReference<Activity> activityWeakReference;
    private ImageView imageView;

    private LruCache<String, Bitmap> mMemoryCache;

    public GalleryImageAdapter(List<String> mImagePath, WeakReference<Activity> weakReference) {
        this.mImagePath = mImagePath;
        this.activityWeakReference = weakReference;

        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null && bitmap != null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    @Override
    public int getCount() {
        return mImagePath.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class GridViewItemHolder {
        ImageView imageView;
        TextView textView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ImageView picture;
        TextView name;

        GridViewItemHolder holder;

        LayoutInflater inflater = (LayoutInflater) activityWeakReference.get().getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {  // if it's not recycled, initialize some attributes

            holder = new GridViewItemHolder();

            // get layout from mobile.xml
            v = inflater.inflate(R.layout.grid_view_item, parent,false);

            holder.imageView = (ImageView) v.findViewById(R.id.picture);
//            holder.textView = (TextView) v.findViewById(R.id.text);

            v.setTag(holder);
//            imageView.setLayoutParams(new GridView.LayoutParams(300, 300));
//            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            holder.imageView.setPadding(8, 8, 8, 8);
        } else {
            holder = (GridViewItemHolder) v.getTag();
        }

        loadBitmap(mImagePath.get(position),holder.imageView);
//        holder.textView.setText(mImagePath.get(position));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activityWeakReference.get().getApplicationContext(), "Clicked -> " + mImagePath.get(position),Toast.LENGTH_SHORT).show();
                // Create and show the dialog.
                PictureDialog newFragment = PictureDialog.newInstance(mImagePath.get(position));
                newFragment.show(activityWeakReference.get().getFragmentManager(),null);
            }
        });

        return holder.imageView;
    }

    private void loadBitmap(String imagePath, ImageView imageView){

        final String imageKey = imagePath;
        final Bitmap bitmap = getBitmapFromMemCache(imageKey);

        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
        else {
            BitmapWorkerTask task = new BitmapWorkerTask(imageView);
            task.execute(imagePath);
        }
    }

    class BitmapWorkerTask extends AsyncTask<String, Void, MySimpleBitmap> {
        private final WeakReference<ImageView> imageViewReference;
        private String data = "";

        public BitmapWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        // Decode image in background.
        @Override
        protected MySimpleBitmap doInBackground(String... params) {

            data = params[0];
            final Bitmap bitmap = ImageUtils.decodeSampledBitmapFromResource(params[0],100,100);

            addBitmapToMemoryCache(params[0],bitmap);

            return MySimpleBitmap.get(bitmap,params[0]);
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(final MySimpleBitmap mySimpleBitmap) {
            if (imageViewReference != null && mySimpleBitmap.getmBitmap() != null) {
                final ImageView imageView = imageViewReference.get();
                if (imageView != null) {
                    imageView.setImageBitmap(mySimpleBitmap.getmBitmap());
                }
            }
        }
    }

}
