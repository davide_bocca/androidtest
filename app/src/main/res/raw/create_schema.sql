CREATE TABLE UTENTI (
    _id integer PRIMARY KEY AUTOINCREMENT UNIQUE,
    username varchar(20) NOT NULL UNIQUE,
    email varchar(50),
    birthdate date,
    location varchar(250),
    password varchar(50) NOT NULL
);
CREATE INDEX UNIQUE_USER_NAME on USERS_INDEX (_id,username);